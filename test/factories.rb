FactoryGirl.define do
    sequence :token do |n|
       "16fc1d86-7d6e-4011-9b75-d6cd9501fe1#{n}"
    end

    factory :bug do
        application_token {generate :token}
        status 'new'
        priority 'minor'
        comment 'new comment'
        number 1
    end

    factory :state do
        bug {create(:bug)}
        device 'iPhone 5'
        os 'iOS'
        memory 1024
        storage 20480
    end
end