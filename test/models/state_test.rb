require 'test_helper'

class StateTest < ActiveSupport::TestCase
  context 'state model' do
    should belong_to :bug
    should validate_presence_of :device
    should validate_presence_of :os
    should validate_presence_of :memory
    should validate_presence_of :storage
  end
end
