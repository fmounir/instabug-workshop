require 'test_helper'

class BugTest < ActiveSupport::TestCase
    context 'bug model' do
      should validate_presence_of :application_token
      should validate_presence_of :number
      should validate_presence_of :status
      should validate_presence_of :priority
    end
end
