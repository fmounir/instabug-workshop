require 'test_helper'


class BugsControllerTest < ActionDispatch::IntegrationTest
  context "bugs api action" do
    setup do
      @bug1_app1 = FactoryGirl.create(:bug , application_token: "16fc1d86-7d6e-4011-9b75-d6cd9501fe1a" , number: 1)
      @bug2_app1 = FactoryGirl.create(:bug , application_token: "16fc1d86-7d6e-4011-9b75-d6cd9501fe1a" , number: 2)

      @bug1_app2 = FactoryGirl.create(:bug , application_token: "16fc1d86-7d6e-4011-9b75-d6cd9501fe1b" , number: 1)
      @bug2_app2 = FactoryGirl.create(:bug , application_token: "16fc1d86-7d6e-4011-9b75-d6cd9501fe1b" , number: 2)

      @state1_app1 =FactoryGirl.create(:state , bug: @bug1_app1)
      @state2_app1 =FactoryGirl.create(:state , bug: @bug2_app1)
      @state1_app2 =FactoryGirl.create(:state , bug: @bug1_app2)
      @state2_app2 =FactoryGirl.create(:state , bug: @bug2_app2)
    end

   should "create new bug" do
    assert_difference ['Bug.count','State.count'] do
      post bugs_path , params: { bug:{
            application_token: "16fc1d86-7d6e-4011-9b75-d6cd9501fe1e",
            status: "new",
            priority: "minor",
            comment: "new comment about bug",
            state: {
              device: "iPhone 6s",
              os: "iOS",
              memory: 1024,
              storage: 20480
              }
            }
      } , as: :json
    end

    bug = Bug.last
    number = assigns(:number)
    assert_response :success
    assert_not_nil bug.number
    assert_equal bug.state.device , "iPhone 6s"
    assert_equal number , Bug.where(application_token: '16fc1d86-7d6e-4011-9b75-d6cd9501fe1e').count
    assert_equal response.body , "{number: #{bug.number}}"
   end

   should "return bug by application_token and number" do
      get "/bugs/#{@bug2_app2.number}" , params: {application_token: @bug2_app2.application_token}
      assert_equal response.body , @bug2_app2.to_json
   end

   should "return bugs count" do
      Rails.cache.delete("count_#{@bug2_app2.application_token}")
      get "/bugs/count/#{@bug2_app2.application_token}"
      count = assigns(:count)
      bugs = Bug.where(application_token: @bug2_app2.application_token)
      assert_equal count , bugs.count
      assert_equal response.body , "{count: #{bugs.count}}"
   end

   should "find cache when count and delete when create" do
    Rails.cache.delete("count_#{@bug2_app2.application_token}")
    get "/bugs/count/#{@bug2_app2.application_token}"
    bugs = Bug.where(application_token: @bug2_app2.application_token)

    assert_equal Rails.cache.fetch("count_#{@bug2_app2.application_token}") , bugs.count

    post bugs_path , params: { bug:{
            application_token: @bug2_app2.application_token,
            status: "new",
            priority: "minor",
            comment: "new comment about bug",
            state: {
              device: "iPhone 6s",
              os: "iOS",
              memory: 1024,
              storage: 20480
              }
            }
      } , as: :json

     assert_equal Rails.cache.fetch("count_#{@bug2_app2.application_token}") , nil
   end
end

    context "elastic search " do
      setup do
        # using fixtures data for elastic search to index
        Bug.import
        Bug.__elasticsearch__.refresh_index!
      end

      should "elastic search by application_token zzzz" do
        get "/bugs" , params: { query: "16fc1d86-7d6e-4011-9b75-d6cd9501zzzz" }
        search_results =  assigns(:result_bugs)
        assert_equal search_results.count , 2
      end

      should "elastic search by application_token aaaaa" do
        get "/bugs" , params: { query: "16fc1d86-7d6e-4011-9b75-d6cd9501aaaaa"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

      should "elastic search by application_token bbbb" do
        get "/bugs" , params: { query: "16fc1d86-7d6e-4011-9b75-d6cd9501bbbb"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 1
      end

      should "elastic search by exact application_token zzzzK" do
        get "/bugs" , params: { query: "16fc1d86-7d6e-4011-9b75-d6cd9501zzzzK" }
        search_results =  assigns(:result_bugs)
        assert_equal search_results.count , 0
      end

      should "elastic search by status 'closed' " do
        get "/bugs" , params: { query: "closed"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 1
      end

     should "elastic search by status 'In-progress' " do
        get "/bugs" , params: { query: "In-progress"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 2
      end

     should "elastic search by priority 'major ' " do
        get "/bugs" , params: { query: "major"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

      should "elastic search by complete comment phrase" do
        get "/bugs" , params: { query: "no comment"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3

        get "/bugs" , params: { query: "lorem ipsum"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

      should "elastic search by leading partial match of comment" do
        get "/bugs" , params: { query: "lore"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

      should "elastic search by partial match of comment" do
        get "/bugs" , params: { query: "ore"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

      should "elastic search by partial match of comment 3 letters" do
        get "/bugs" , params: { query: "psu"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

      should "elastic search by trail partial match of comment" do
        get "/bugs" , params: { query: "ipsu"}
        search_results = assigns(:result_bugs)
        assert_equal search_results.count , 3
      end

    end

    context "bugs api unhappy scenarios" do
      should "show error when bug param is missing" do
        post "/bugs"
        assert_equal response.body , "{ error: Bug param is missing }"
        assert_equal response.status , 406
      end

      should "show error when state param is missing" do
        post "/bugs" ,params: { bug: { application_token: "16fc1d86-7d6e-4011-9b75-d6cd9501aaa1", status: "new", priority: "minor", comment: "new comment about bug"}
                      } , as: :json

        assert_equal response.body , "{ error: State param is missing }"
        assert_equal response.status , 406
      end

       should "show error when one param is missing (application token )" do
        post "/bugs" , params: { bug:{
            status: "new",
            priority: "minor",
            comment: "new comment about bug",
            state: {
              device: "iPhone 6s",
              os: "iOS",
              memory: 1024,
              storage: 20480
              }
            }
      } , as: :json

        assert_equal response.body , "{ error: [\"Application token can't be blank\"] - [] }"
        assert_equal response.status , 406
      end

    should "show error when state params is missing ( device )" do
        post "/bugs" , params: { bug:{
            application_token: "16fc1d86-7d6e-4011-9b75-22222",
            status: "new",
            priority: "minor",
            comment: "new comment about bug",
            state: {
              os: "iOS",
              memory: 1024,
              storage: 20480
              }
            }
      } , as: :json

        assert_equal response.body , "{ error: [] - [\"Device can't be blank\"] }"
        assert_equal response.status , 406
      end

     should "show error when searching bugs without a query" do
        get "/bugs"
        assert_equal response.body , "{ error: Query is missing }"
        assert_equal response.status , 406
     end

     should "show error when number exists but application token not" do
      get "/bugs/1"
      assert_equal response.body , "{ error: Application token is missing }"
      assert_equal response.status , 406
   end
    end
end
