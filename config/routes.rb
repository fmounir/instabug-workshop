Rails.application.routes.draw do
  get '/bugs/:id' , to: 'bugs#show'
  post '/bugs' , to: 'bugs#create'
  get '/bugs/count/:application_token' , to: 'bugs#count'
  get '/bugs' , to: 'bugs#search'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
