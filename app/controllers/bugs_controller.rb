class BugsController < ApplicationController
  require 'exceptions'
  def create
    begin
    @number = Bug.create_bug(bug_fields , state_fields[:state])
    render json: "{number: #{@number}}"
    rescue => exception
      render_error(exception)
    end
  end

  def show
    begin
    bug = Bug.get_bug(params[:id] , params[:application_token])
    render json: bug
    rescue => exception
      render_error(exception)
    end
  end

  def count
    @count = Bug.count_bugs(params[:application_token])
    render json: "{count: #{@count}}"
  end

  def search
    begin
      @result_bugs = Bug.search_bugs(params[:query])
      render json: @result_bugs
    rescue => exception
      render_error(exception)
    end
  end

private
  def render_error(ex)
    render json: "{ error: #{ex.reason} }" , status: 406
  end

  def bug_fields
    params.fetch(:bug , {}).permit(:application_token , :number , :status , :priority , :comment)
  end

  def state_fields
    params.fetch(:bug , {}).permit(:state => [:device , :os , :memory , :storage])
  end
end
