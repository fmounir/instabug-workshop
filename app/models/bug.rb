require 'elasticsearch/model'
require "bunny"

class Bug < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  require 'exceptions'

  has_one :state , :dependent => :destroy
  validates_presence_of :application_token
  validates_presence_of :number
  validates_presence_of :status
  validates_presence_of :priority

  def self.create_bug(bug_fields , state_fields)
    check_params(bug_fields , state_fields)
    write_bug(bug_fields , state_fields)
  end

  def self.get_bug(number , application_token)
    check_token(application_token)
    bug = Bug.where("number = :number AND application_token = :application_token", number: number , application_token: application_token).first
  end

  def self.count_bugs(application_token)
    Rails.cache.fetch("count_#{application_token}") {Bug.where(application_token: application_token).count}
  end

  def self.search_bugs(query)
    if !query.present?
      raise Exceptions::MissingParam.new("Query is missing")
    end
    result = Bug.search(query: { bool: { should: [{ match_phrase: { _all: query } } , { fuzzy: { comment: { value: query , fuzziness: 2 }  } }] } }).records.to_a
    result
  end

 private

  def self.check_token(application_token)
    if !application_token.present?
      raise Exceptions::MissingParam.new("Application token is missing")
    end
  end

  def self.raise_save_errors(bug , state)
    error = "#{bug.errors.full_messages} - #{state.errors.full_messages}"
    raise Exceptions::MissingParam.new(error)
  end

  def self.check_params(bugs , state)
      if !bugs.present?
        raise Exceptions::MissingParam.new("Bug param is missing")
      end
      if !state.present?
        raise Exceptions::MissingParam.new("State param is missing")
      end
  end

  def self.generate_number(application_token)
     application_bugs = Bug.where(:application_token => application_token)
     application_bugs.count + 1
  end

  def self.write_bug(bug_fields , state_fields)
    bug = Bug.new(bug_fields)
    bug.number = generate_number(bug.application_token)
    state = State.new(state_fields)
    bug.state = state

    if bug.save && state.save
      Rails.cache.delete("count_#{bug.application_token}")
      bug.number
      else
      raise_save_errors(bug , state)
    end
  end
end
