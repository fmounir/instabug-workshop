class State < ApplicationRecord
  belongs_to :bug
  validates_presence_of :device
  validates_presence_of :os
  validates_presence_of :memory
  validates_presence_of :storage
end
