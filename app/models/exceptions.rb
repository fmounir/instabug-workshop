module Exceptions
    class Exception < StandardError
        attr_reader :reason
        def initialize(reason)
          @reason = reason
        end
    end
    class MissingParam < Exception
    end
end